<?php

defined('_JEXEC') or die;

// variables
$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$menu = $app->getMenu();
$active = $app->getMenu()->getActive();
$params = $app->getParams();
$pageclass = $params->get('pageclass_sfx');
$tpath = $this->baseurl . '/templates/' . $this->template;
$templateparams = $app->getTemplate(true)->params;

// generator tag
$this->setGenerator(null);



// force latest IE & chrome frame
$doc->setMetadata('x-ua-compatible', 'IE=edge,chrome=1');

// JQUERY
JHtml::_('jquery.framework');
// BOOTSTRAP

$doc->addScript($tpath . '/js/bootstrap.min.js');
$doc->addScript($tpath . '/js/logic.js'); // <- use for custom script
$doc->addScript($tpath . '/js/jquery.lazyload.js');
$doc->addScript($tpath . '/js/parallax.min.js');

// CSS
$doc->addStyleSheet('https://use.fontawesome.com/releases/v5.7.0/css/all.css');

// CSS TEMPLATE

$doc->addStyleSheet($tpath . '/css/bootstrap.css');
$doc->addStyleSheet($tpath . '/css/style.css?20181120');

