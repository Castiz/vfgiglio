<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
//require_once JPath::clean(JPATH_ROOT . "/components/com_master/helpers/helper.php");
?>

<!-- Swiper -->

<!--<div class="mod_menu"  id="header_position"></div>-->
<div class="mod_menu" id="mod_menu">
    <div class="menu-button line" style="float:left">
        <div class="bar red"></div>
        <div class="bar red"></div>
        <div class="bar red"></div>
    </div>
    <!--<div style="position:absolute; top: 70px"><img src="/templates/master/images/logo_reverse_red.png" height="50" style="margin:0 14px 14px 14px" ></div>-->
    <!--<div class="logo"><a href="/"><img src="/images/home/logo.png" style="float:left" height="50"></a></div>-->
    <div class="clear"></div>
    <div class="menu-container">
        <div class="menu">
            <?php // The menu class is deprecated. Use nav instead.  ?>
            <ul class="nav menu<?php echo $class_sfx; ?>"<?php
                $tag = '';
                if ($params->get('tag_id') != null)
                {
                    $tag = $params->get('tag_id') . '';
                    echo ' id="' . $tag . '"';
                }
                ?>>
                    <?php
                    foreach ($list as $i => &$item)
                    {
//                        if ($item->alias == "projects")
//                        {
//                            if (!MasterAuthHelper::canViewJob(NULL))
//                            {
//                               continue;
//                            }
//                        }
                        $class = 'item-' . $item->id;
                        if (($item->id == $active_id) OR ( $item->type == 'alias' AND $item->params->get('aliasoptions') == $active_id))
                        {
                            $class .= ' current';
                        }
                        if (in_array($item->id, $path))
                        {
                            $class .= ' active';
                        }
                        elseif ($item->type == 'alias')
                        {
                            $aliasToId = $item->params->get('aliasoptions');

                            if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
                            {
                                $class .= ' active';
                            }
                            elseif (in_array($aliasToId, $path))
                            {
                                $class .= ' alias-parent-active';
                            }
                        }

                        if ($item->type == 'separator')
                        {
                            $class .= ' divider';
                        }

                        if ($item->deeper)
                        {
                            $class .= ' deeper';
                        }

                        if ($item->parent)
                        {
                            $class .= ' parent';
                        }

                        if (!empty($class))
                        {
                            $class = ' class="' . trim($class) . '"';
                        }

//                                echo '<li' . $class . ' style="width:' . floor(100 / count($list)) . '%">';
                        echo '<li' . $class . '>';

                        // Render the menu item.
                        switch ($item->type) :
                            case 'separator':
                            case 'url':
                            case 'component':
                            case 'heading':
                                require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
                                break;

                            default:
                                require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
                                break;
                        endswitch;

                        // The next item is deeper.
                        if ($item->deeper)
                        {
                            echo '<ul class="nav-child unstyled small">';
                        }
                        elseif ($item->shallower)
                        {
                            // The next item is shallower.
                            echo '</li>';
                            echo str_repeat('</ul></li>', $item->level_diff);
                        }
                        else
                        {
                            // The next item is on the same level.
                            echo '</li>';
                        }
                    }
                    ?></ul>
        </div>

    </div>
    <div class="toolbar">
        <a target="_blank" href="https://www.lexia.it/lawyers/avv-vincenzo-fabrizio-giglio/"><img height="30" src="/images/ico/lexia-logo.png" alt="lexia"></a>
        <a target="_blank" href="https://www.linkedin.com/in/vincenzofabriziogiglio/"><img height="30" src="/images/ico/linked_in.png" alt="in"></a>
    </div>
</div>


<script>
    jQuery(window).on('load', function() {
        var header = document.getElementById("mod_menu");
        var header_position = document.getElementById("header_position");
        var menuHeight = jQuery('.mod_menu').height();
        var menuWidth = jQuery('.mod_menu').width();
        var headerTitleWidth = jQuery('#slideshow .header .header_title').width();
        var sticky = 100;
        var menuTitleOffset = jQuery('.header_title').offset().top;
        console.log(menuWidth);
        console.log(headerTitleWidth);
        jQuery(window).scroll(function() {
            console.log(window.pageYOffset);
            if (window.pageYOffset > menuHeight) {
//                jQuery("#slideshow .header img").css("padding-top", menuHeight);
//                jQuery(".mod_menu").hide();
//                jQuery(".mod_menu").addClass("fix");

            } else {
//                jQuery(".mod_menu").stop();
//                jQuery(".mod_menu").show();
//                jQuery(".mod_menu").removeClass("fix");
                jQuery("#slideshow .header img").css("padding-top", "0");
            }
            if (window.pageYOffset > menuTitleOffset) {
//                jQuery(".mod_menu").stop();
                jQuery("#slideshow .header .header_title").stop();
                jQuery("#slideshow .header .header_title").addClass("fix");
//                jQuery("#slideshow .header .header_title").animate({'width': '100%'}, "fast");
//                jQuery("#slideshow .header .header_title").animate().css({'border-bottom': '1px solid #f00'});
//                jQuery(".mod_menu").animate({'width': '100%'}, "fast");
//                jQuery(".mod_menu").css({'border-bottom': '1px solid #f00'});
                ;
            } else
            {
                jQuery(".mod_menu").stop();
                jQuery("#slideshow .header .header_title").stop();
                jQuery("#slideshow .header .header_title").removeClass("fix");
//                jQuery("#slideshow .header .header_title").animate({'width': headerTitleWidth + 70}, "fast");
//                 jQuery("#slideshow .header .header_title").animate().css({'border-bottom': 'none'});
//                jQuery(".mod_menu").animate({'width': menuWidth}, 'fast');
            }
//            var aTop = $('.ad').height();
//            if (jQuery(this).scrollTop() >= aTop) {
//                alert('header just passed.');
//                // instead of alert you can use to show your ad
//                // something like $('#footAd').slideup();
//            }
        });
    });
</script>
<!-- Initialize Swiper -->
<script>
    var open = 0;
    jQuery('.menu-button.line').click(function() {
        var menuWidth = '100%';
        console.log(menuWidth);
        jQuery(".menu-button").addClass("cross");
        jQuery(".menu-button").removeClass("line");
//            jQuery(".menu-button").removeClass(".menu-button");
        jQuery(".menu-container").slideToggle("fast", function() {
            // Animation complete.
        });
        if (open === 0)
        {
            jQuery(".mod_menu").animate({'width': '100%'}, "fast");
            open = 1;
        } else
        {
            jQuery(".menu-button").removeClass("cross");
            jQuery(".mod_menu").animate({'width': menuWidth}, "fast");
            open = 0;
        }
    });

    jQuery('.menu-button.cross').click(function() {

        jQuery(".mod_menu").animate({'width': '100%'}, "fast");
        jQuery(".menu-button").addClass("line");
        jQuery(".menu-button").removeClass("cross");
    });
</script>