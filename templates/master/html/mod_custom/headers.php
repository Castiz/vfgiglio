<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

$active = JFactory::getApplication()->getMenu()->getActive();

?>

<div class="header">
    
    <div class="header_title"> <span><?php echo $active->title;?></span></div>
         <img class="img-responsive" src="images/headers/<?php echo $active->id;?>" alt="<?php echo $active->title;?>" />
             <!--<iframe  style="width: 100%" class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12228.03038340317!2d9.167466999999998!3d45.46356500000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!3m2!1sen!2sus!4v1559048412523!5m2!1sen!2sus" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->
</div>