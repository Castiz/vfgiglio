<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
<style>
    .where{ background: #3D3C3F; color: #e1e1e1; padding: 4rem;}
    .where .box {
    }
    .where .box .main{
        font-size: 5rem;
    }
    .where .box .sub{
        font-size: 1.6rem;
    }
    .legal{
        text-align: center;
        padding: 1rem;
    }
</style>

<div class="where  row">
    <div class="box container "> 
        <!--<div class="main">{article 4}[title]{/article} </div>--> 
        <div class="col-xs-12 col-sm-6 col-md-4 col-xl-4"> 
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5596.619388838502!2d9.167467!3d45.463565!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4786c15967eb42d5%3A0x396ee3152eb0949c!2sVia+San+Vittore%2C+45%2C+20123+Milano+MI%2C+Italy!5e0!3m2!1sen!2sus!4v1556810940527!5m2!1sen!2sus" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>   
        <div class="col-xs-12 col-sm-6 col-md-8 col-xl-8">  
            <div class="sub">
                {article 4}[text]{/article}   
            </div> 
        </div> 
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 legal "> 
    © 2019 - Avv. Vincenzo Fabrizio Giglio – Studio legale del Lavoro – P.IVA 02043780812
</div>
