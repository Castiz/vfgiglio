<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
<style>
    .claim{}
    .claim .content {
        background: #A7032C;
        padding: 20px 0;
        margin: 200px 0;
        text-align: center;
    }
    .claim .content .main{
        font-size: 5rem;
    }
    .claim .content .sub{
        font-size: 2rem;
    }
</style>

<div class="claim">
    <div class="col-md-2"></div>
    <div class="col-md-8 content">
        <div class="main">Lorem Ipsum</div>    
        <div class="sub">Lorem Ipsum</div>  
    </div>
    <div class="col-md-2"></div>
</div>
