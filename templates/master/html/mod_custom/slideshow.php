<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>

<div class="parallax-window" data-parallax="scroll" data-bleed="10" data-z-index="-999" data-image-src="/images/headers/slide_hp.jpg" id="hp_header_container">
    <div class="slideshow">
        <div>
            <img class="slide"  src="images/headers/slide_hp.jpg"/>                
        </div>
    </div>
</div>
