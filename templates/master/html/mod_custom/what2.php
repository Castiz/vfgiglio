<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
<style>
    .what2{ padding: 4rem 0}
    .what2 .box {
        background: #933644;
        padding: 30px ;
        margin: 5px 0;
        text-align: center;
        color: #fff;
        min-height: 250px;
    }
    .what2 .box .main{
        font-size: 3rem;
    }
    .what2 .box .sub{
        font-size: 1.2rem;
    }
</style>

<div class="what2  container">
    <div class="col-xs-4 col-sm-4 col-md-4 col-xl-4"> 
        <div class="box"> 
            <div class="main"> {article 1}[title]{/article} </div>    
            <div class="sub"> {article 1}[introtext]{/article} </div>  
            <div class="sub"> {article 1}[link]+[/link]{/article} </div>  
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-xl-4"> 
        <div class="box"> 
            <div class="main">{article 2}[title]{/article}</div>    
            <div class="sub"> {article 2}[introtext]{/article} </div>
              <div class="sub"> {article 2}[link]+[/link]{/article} </div>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-xl-4"> 
        <div class="box"> 
            <div class="main">{article 3}[title]{/article}</div>    
            <div class="sub"> {article 3}[introtext]{/article} </div>  
              <div class="sub"> {article 3}[link]+[/link]{/article} </div>
        </div>
    </div>
</div>
