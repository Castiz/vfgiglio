<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
<style>
    .claim{}
    .claim .picture{  text-align: center; margin-top: 80px; height: 150px; }
    .claim .picture img{  text-align: center;  }
    .claim .content {
        background: #333;
        padding: 20px 0;
        opacity: 0.6;
        margin: 80px 0;
        text-align: center;
        color: #fff;
        /*border: 5px solid red;*/

    }
    .claim .content .main{
        font-size: 5rem;

    }
    .claim .content .sub{
        font-size: 2rem;
    }
</style>
<div class="parallax-window" data-parallax="scroll" data-bleed="10" data-z-index="-999" data-image-src="/images/headers/slide_hp.jpg" id="hp_header_container">
    <div class="claim row">
        <div class="col-md-12 picture row">
            <img  class=" " src="/images/headers/vfg_new_square.png">
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-8  content">
            <div class="main">Vincenzo Fabrizio Giglio</div>    
            <div class="sub">Avvocato del lavoro dal 2002</div>  
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
