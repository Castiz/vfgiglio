<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
<style>
    .what{ padding: 4rem}
    .what.intro .box {
        background: none;
        margin: 5px 0;
        text-align: justify;
        color: #666;
        /*min-height: 430px;*/
    }
    .what.intro .box .main{
        font-size: 5rem;
        text-align: center
    }
    .what.intro .box .sub{
        font-size: 1.4rem;
        text-align: center;
        color: #333;
    }
    .what.activities .box {
        background: none;
        padding: 80px 30px ;
        margin: 5px 0;
        text-align: center;
        color: #ddd;
        border: 1px solid #e1e1e1;
        /*min-height: 430px;*/
    }
    .what.activities{
        /*background: #ddd;*/
        /*box-shadow: 1px 0 10px #333;*/
        color: #ccc;
        padding: 0 0 4rem 0;
        margin: 10px 0;
    }
      .what.activities .box{
        background: #fff;
    }
    .what.activities .box .main{
        font-size: 2rem;
    }
    .what.activities .box .sub{
        font-size: 2rem;
    }
</style>

<div class="what container intro">
    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12"> 
        <div class="box "> 
            <div class="main">Attività</div>    
<!--            <div class="sub">
                Mi occupo solo di diritto del lavoro.<br>
                Presto assistenza sia ai datori di lavoro, italiani e stranieri, sia alle persone fisiche (dirigenti, quadri, impiegati, operai e agenti di commercio), offrendo un supporto altamente specializzato in ogni esigenza di gestione delle Risorse Umane.<br>
                L'assistenza si svolge nella consulenza day-by-day o nella gestione di singole azioni o problemi complessi: a monte dei problemi per evitare che essi sorgano (il miglior modo per evitare una lite è la stipulazione di un buon contratto; il miglior modo per non perderla è gestirla in modo professionale fin dall'inizio); nelle controversie insorte operando per la ricerca di una soluzione condivisa tra le parti, evitando fin dove è possibile i rischi e i costi di una lite giudiziaria; in giudizio per la tutela dei nostri clienti e dei loro interessi ogni qualvolta ciò sia necessario o opportuno.   
            </div>  -->
        </div>
    </div>
</div>
<div class="what activities row">
    <div class="col-xs-3 col-sm-3 col-md-3 col-xl-3"> 
        <div class="box"> 
            <div class="main">Consulenza</div>    
        </div>
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-xl-3"> 
        <div class="box"> 
            <div class="main">Lavoro Subordinato</div>    
        </div>
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-xl-3"> 
        <div class="box"> 
            <div class="main">Gestione del rapporto</div>    
        </div>
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-xl-3"> 
        <div class="box"> 
            <div class="main">Relazioni Sindacali</div>    
        </div>
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-xl-3"> 
        <div class="box"> 
            <div class="main">Consulenza</div>    
        </div>
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-xl-3"> 
        <div class="box"> 
            <div class="main">Lavoro Subordinato</div>    
        </div>
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-xl-3"> 
        <div class="box"> 
            <div class="main">Gestione del rapporto</div>    
        </div>
    </div>
    <div class="col-xs-3 col-sm-3 col-md-3 col-xl-3"> 
        <div class="box"> 
            <div class="main">Relazioni Sindacali</div>    
        </div>
    </div>
</div>
