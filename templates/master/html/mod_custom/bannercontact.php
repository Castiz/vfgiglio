<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
<style>
    .bannercontact{}
    .bannercontact .box {
        background: #A7032C;
        padding: 20px 0;
        margin: 50px 0;
        text-align: center;
    }
    .bannercontact .box .main{
        font-size: 5rem;
    }
    .bannercontact .box .sub{
        font-size: 2rem;
    }
</style>

<div class="bannercontact row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12"> 
        <div class="box row"> 
            <div class="main col-xs-8 col-sm-8 col-md-8 col-xl-8">Contattaci</div>    
            <div class="sub  col-xs-4 col-sm-4 col-md-4 col-xl-4">029989589</div>  
        </div>
    </div>
</div>
