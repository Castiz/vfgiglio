<?php
defined('_JEXEC') or die;

include_once JPATH_THEMES . '/' . $this->template . '/logic.php';
?>
<!doctype html>

<html lang="<?php echo $this->language; ?>">
    <head>
    <jdoc:include type="head" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,400,400i,600,600i&display=swap" rel="stylesheet">
    
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $tpath; ?>/images/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $tpath; ?>/images/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $tpath; ?>/images/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $tpath; ?>/images/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $tpath; ?>/images/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $tpath; ?>/images/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $tpath; ?>/images/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $tpath; ?>/images/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $tpath; ?>/images/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $tpath; ?>/images/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $tpath; ?>/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $tpath; ?>/images/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $tpath; ?>/images/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo $tpath; ?>/images/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
    
    <!-- Le HTML5 shim and media query for IE8 support -->
    <!--[if lt IE 9]>
                <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <script type="text/javascript" src="<?php echo $tpath; ?>/js/respond.min.js"></script>
                <![endif]-->
</head>

<body>
 <div id="modal_loading" class="preload"><div class="spinner"><i class="fa fa-spinner fa-spin"></i></div></div>
    <div id="header"> 
        <div class="row">
            <div class="">
                <!--                <div class="logo col-xs-11 col-sm-3 col-md-3 col-lg-3">
                                    <a href="<?php echo JURI::root(); ?>">
                                        <img class="img-responsive" alt="Logo" src="<?php echo $tpath; ?>/images/logo.png" />
                                    </a>
                                </div>-->
                <!-- Navbar	-->
                <?php if ($this->countModules('navbar')) : ?>
                    <div id="navbar" class="">
                        <div class="row">
                            <div class="">
                                <jdoc:include type="modules" name="navbar" style="block"/>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!--Slideshow-->
    <?php if ($this->countModules('slideshow')) : ?>
        <div id="slideshow">
            <div class="row">
                <div class="">
                    <jdoc:include type="modules" name="slideshow" style="block"/>
                    <div class="clear">
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <!--Main Body-->
    <div id="main">
        <div id="component">
            <div class="container">
                <div class="component">
                    <div class="boxed">
                        <jdoc:include type="component"/>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>

        <!--Top -->
        <?php if ($this->countModules("top")) : ?>
            <div id="top">
                <div class="row">
                    <div class="">
                        <jdoc:include type="modules" name="top" style="block" />
                    </div>
                </div>
            </div>
        <?php endif ?>

        <!--Middle -->
        <?php if ($this->countModules("middle")) : ?>
            <div id="middle">
                <div class="row">
                    <div class="">
                        <jdoc:include type="modules" name="middle" style="block" />
                    </div>
                </div>
            </div>
        <?php endif ?>

        <!--Bottom -->
        <?php if ($this->countModules("bottom")) : ?>
            <div id="bottom">
                <div class="row">
                    <div class="">
                        <jdoc:include type="modules" name="bottom" style="block" />
                    </div>
                </div>
            </div>
        <?php endif ?>

        <!--Footer -->
        <?php if ($this->countModules("footer")) : ?>
            <div id="footer">
                <div class="row">
                    <div class="">
                        <jdoc:include type="modules" name="footer" style="block" />
                    </div>
                </div>
            </div>
        <?php endif ?>

    </div>
<jdoc:include type="modules" name="debug" />
</body>
</html>

<script type="text/javascript">
     jQuery(document).ready(function() {
        jQuery(".preload").fadeOut(1000);
        jQuery("body").fadeIn(1000);
    });
</script>

<!--GOOGLE ANALYTICS -->
<script>

</script>
<!--END GOOGLE ANALYTICS -->