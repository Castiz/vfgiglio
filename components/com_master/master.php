<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Master
 * @author     Luca <casti.luca@tiscali.it>
 * @copyright  2019 Luca
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Master', JPATH_COMPONENT);
JLoader::register('MasterController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Master');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
