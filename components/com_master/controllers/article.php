<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Master
 * @author     Luca <casti.luca@tiscali.it>
 * @copyright  2019 Luca
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Article controller class.
 *
 * @since  1.6
 */
class MasterControllerArticle extends JControllerLegacy {

    /**
     * Method to check out an item for editing and redirect to the edit form.
     *
     * @return void
     *
     * @since    1.6
     */
    public function edit()
    {
        $app = JFactory::getApplication();

        // Get the previous edit id (if any) and the current edit id.
        $previousId = (int) $app->getUserState('com_master.edit.article.id');
        $editId = $app->input->getInt('id', 0);

        // Set the user id for the user to edit in the session.
        $app->setUserState('com_master.edit.article.id', $editId);

        // Get the model.
        $model = $this->getModel('Article', 'MasterModel');

        // Check out the item
        if ($editId)
        {
            $model->checkout($editId);
        }

        // Check in the previous user.
        if ($previousId && $previousId !== $editId)
        {
            $model->checkin($previousId);
        }

        // Redirect to the edit screen.
        $this->setRedirect(JRoute::_('index.php?option=com_master&view=articleform&layout=edit', false));
    }

    /**
     * Method to save a user's profile data.
     *
     * @return    void
     *
     * @throws Exception
     * @since    1.6
     */
    public function publish()
    {
        // Initialise variables.
        $app = JFactory::getApplication();

        // Checking if the user can remove object
        $user = JFactory::getUser();

        if ($user->authorise('core.edit', 'com_master') || $user->authorise('core.edit.state', 'com_master'))
        {
            $model = $this->getModel('Article', 'MasterModel');

            // Get the user data.
            $id = $app->input->getInt('id');
            $state = $app->input->getInt('state');

            // Attempt to save the data.
            $return = $model->publish($id, $state);

            // Check for errors.
            if ($return === false)
            {
                $this->setMessage(JText::sprintf('Save failed: %s', $model->getError()), 'warning');
            }

            // Clear the profile id from the session.
            $app->setUserState('com_master.edit.article.id', null);

            // Flush the data from the session.
            $app->setUserState('com_master.edit.article.data', null);

            // Redirect to the list screen.
            $this->setMessage(JText::_('COM_MASTER_ITEM_SAVED_SUCCESSFULLY'));
            $menu = JFactory::getApplication()->getMenu();
            $item = $menu->getActive();

            if (!$item)
            {
                // If there isn't any menu item active, redirect to list view
                $this->setRedirect(JRoute::_('index.php?option=com_master&view=articles', false));
            }
            else
            {
                $this->setRedirect(JRoute::_('index.php?Itemid=' . $item->id, false));
            }
        }
        else
        {
            throw new Exception(500);
        }
    }

    /**
     * Remove data
     *
     * @return void
     *
     * @throws Exception
     */
    public function remove()
    {
        // Initialise variables.
        $app = JFactory::getApplication();

        // Checking if the user can remove object
        $user = JFactory::getUser();

        if ($user->authorise('core.delete', 'com_master'))
        {
            $model = $this->getModel('Article', 'MasterModel');

            // Get the user data.
            $id = $app->input->getInt('id', 0);

            // Attempt to save the data.
            $return = $model->delete($id);

            // Check for errors.
            if ($return === false)
            {
                $this->setMessage(JText::sprintf('Delete failed', $model->getError()), 'warning');
            }
            else
            {
                // Check in the profile.
                if ($return)
                {
                    $model->checkin($return);
                }

                $app->setUserState('com_master.edit.inventory.id', null);
                $app->setUserState('com_master.edit.inventory.data', null);

                $app->enqueueMessage(JText::_('COM_MASTER_ITEM_DELETED_SUCCESSFULLY'), 'success');
                $app->redirect(JRoute::_('index.php?option=com_master&view=articles', false));
            }

            // Redirect to the list screen.
            $menu = JFactory::getApplication()->getMenu();
            $item = $menu->getActive();
            $this->setRedirect(JRoute::_($item->link, false));
        }
        else
        {
            throw new Exception(500);
        }
    }

    public function import()
    {

        if (version_compare(JVERSION, '3.0', 'lt'))
        {
            JTable::addIncludePath(JPATH_PLATFORM . 'joomla/database/table');
        }
//        $filename = $_FILES["file"]["tmp_name"];
        $filename = JPATH_BASE . "\images\import.csv";
        $file = fopen($filename, "r");
//         $file = str_replace("’", "'", $file);
//          print_r($file);
        while (($getData = fgetcsv($file, 0,";")) !== FALSE)
        {
              if($getData[0] != NULL)
                  {
//           print_r($getData);
                   echo $date = date('Y-m-d H:i:s', strtotime($getData[1]));
                    echo "<br>";
            echo $title = htmlentities($getData[3], ENT_QUOTES | ENT_IGNORE, "ISO8859-1");
            echo "<br>";
            echo $introtext = str_replace("’", "'",htmlentities($getData[2], ENT_QUOTES | ENT_IGNORE, "ISO8859-1"));
            echo $fulltext = str_replace("’", "'",htmlentities($getData[4], ENT_QUOTES | ENT_IGNORE, "ISO8859-1"));
            $text = $introtext.'<br><br>'.$fulltext;
            echo "<br><br>";
           
echo "<br>";
            $article = JTable::getInstance('content');
//            print_r($article);
            $article->title =  $title;
            $article->alias = JFilterOutput::stringURLSafe($title);
            $article->introtext = $text;
//            $article->fulltext = $fulltext;
             $article->created = $date;
            $article->catid = 8;
            $article->created_by_alias = 'Super User';
            $article->state = 1;
            $article->access = 1;
            $article->metadata = '{"page_title":"","author":"","robots":""}';
            $article->language = '*';
// Check to make sure our data is valid, raise notice if it's not.

            if (!$article->check())
            {
                echo "error";
                JError::raiseNotice(500, $article->getError());
print_r($article->getError());
                return FALSE;
            }

// Now store the article, raise notice if it doesn't get stored.
            if (!$article->store(TRUE))
            {
                JError::raiseNotice(500, $article->getError());

                return FALSE;
            }
            }
        }
    }

}
