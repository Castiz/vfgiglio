<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Master
 * @author     Luca <casti.luca@tiscali.it>
 * @copyright  2019 Luca
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;


?>

<div class="item_fields">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_MASTER_FORM_LBL_ARTICLE_ID_NL'); ?></th>
			<td><?php echo $this->item->id_nl; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_MASTER_FORM_LBL_ARTICLE_ID_PROG'); ?></th>
			<td><?php echo $this->item->id_prog; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_MASTER_FORM_LBL_ARTICLE_ESTREMI_NEWS'); ?></th>
			<td><?php echo $this->item->estremi_news; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_MASTER_FORM_LBL_ARTICLE_INCIPIT'); ?></th>
			<td><?php echo $this->item->incipit; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_MASTER_FORM_LBL_ARTICLE_TITOLO'); ?></th>
			<td><?php echo $this->item->titolo; ?></td>
		</tr>

	</table>

</div>

