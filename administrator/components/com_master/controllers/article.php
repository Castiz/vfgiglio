<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Master
 * @author     Luca <casti.luca@tiscali.it>
 * @copyright  2019 Luca
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Article controller class.
 *
 * @since  1.6
 */
class MasterControllerArticle extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'articles';
		parent::__construct();
	}
}
