<?php

defined('_JEXEC') or die;

class ModMasterHelper {
     // CATEGORIES
    public static function getLatestNews($catId = NULL, $limit = 8 )
    {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__content', 'c'));
         $query->order($db->quoteName('c.created'). ' DESC '); 
        if (!empty($catId))
        {
            $query->where($db->quoteName('c.catid') . ' = (' . $catId . ')');
        }
//        $query->where($db->quoteName('parent_id') . ' != 0');
        $query->where($db->quoteName('c.state') . ' = 1');
       
//        $query->group($db->quoteName('cat.id'));
        $query->setLimit($limit);
        $db->setQuery($query);
        
        return $db->loadObjectList();
    }

  
}