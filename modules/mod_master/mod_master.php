<?php

defined('_JEXEC') or die;

// RESOURCES
require_once __DIR__ . '/helper.php';
JFactory::getLanguage()->load('com_master');

// LAYOUT
$headerText = trim($params->get('header_text'));
$footerText = trim($params->get('footer_text'));
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$latestNews = ModMasterHelper::getLatestNews(8);

// OUTPUT
require JModuleHelper::getLayoutPath('mod_master', $params->get('layout', 'default'));
