<?php
defined('_JEXEC') or die;
?>
<div class="mod_master">
    <div id="barra_spaziatrice" style="display: none; height: 80px;"></div>
    <section class="introduction" style="display:none;">
        <div class="flex-wrapper content-wrapper">
            <div class="claim-container">
<!--                <div class="claim">
                    <img   src="/images/home/infront.png"/>
                </div>-->
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </div>
            <a class="volume" href="javascript:;">
                <i class="on fas fa-volume-up" style="display:none;"></i>
                <i class="off fas fa-volume-mute"></i>
            </a>
            <a class="fullscreen" href="javascript:;">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
            <video id="mod_master_introduction_video"  autoplay muted loop controlsList="" >
                <source src="/images/home/video_presentation/promo_infront.mp4"   type="video/mp4">
                <source src="/images/home/video_presentation/promo_infront.ogg"  type="video/ogg">
            </video>
            <div class="strip-sponsor">
                <div class=""></div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

    jQuery(function() {

        var videoSelector = "#mod_master_introduction_video";
        var toggleSelector = ".mod_master .introduction .volume";
        var fullscreenSelector = ".mod_master .introduction .fullscreen";

        // Toggle volume on click
        jQuery(toggleSelector).click(function() {
            let currentMuted = jQuery(videoSelector).prop('muted');
            if (currentMuted) {
                jQuery(toggleSelector + ' .off').hide();
                jQuery(toggleSelector + ' .on').show();
            } else {
                jQuery(toggleSelector + ' .on').hide();
                jQuery(toggleSelector + ' .off').show();
            }
            jQuery(videoSelector).prop('muted', !currentMuted);
        });

        // Fullscreen on click
        jQuery(fullscreenSelector).click(function() {
            var elem = document.getElementById("mod_master_introduction_video");
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) { /* Firefox */
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
                elem.webkitRequestFullscreen();
            } else if (elem.msRequestFullscreen) { /* IE/Edge */
                // Note: Internet Explorer 10 and earlier does not support the msRequestFullscreen() method.
                elem.msRequestFullscreen();
            }
        });

        // =====================================================================

        function enableDisableClaimVideo() {
            var elem = document.getElementById("mod_master_introduction_video");
            var width = jQuery(window).width();
            if (width >= 992) {
                elem.play();
                jQuery(".mod_master .introduction").show();
                jQuery(".mod_master #barra_spaziatrice").hide();
            } else {
                jQuery(".mod_master .introduction").hide();
                jQuery(".mod_master #barra_spaziatrice").show();
                elem.pause();
            }
        }

        // INIT
        enableDisableClaimVideo();
        // Resize
        jQuery(window).on('resize', function() {
            enableDisableClaimVideo();
        });


    });

</script>
