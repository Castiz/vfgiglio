<?php
defined('_JEXEC') or die;
?>

<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
<style>
    .what{ }
    .what.intro .box {
        background: none;
        margin: 5px 0;
        text-align: justify;
        color: #333;
        /*min-height: 430px;*/
    }
    .what.intro .box .main{
        font-size: 5rem;
        text-align: center
    }
    .what.intro .box .sub{
        font-size: 1.2rem;
        text-align: center;
        color: #333;
    }
    .what.activities .box {
        background: none;
        padding: 30px ;
        margin: 5px 0;
        text-align: left;
        border: 1px solid #e1e1e1;
        /*min-height: 430px;*/
    }
    .what.activities{
        /*background: #ddd;*/
        /*box-shadow: 1px 0 10px #333;*/
        margin: 10px 0;
    }
    .what.activities .box{
        background: #fff;
        min-height: 320px;
    }
    .what.activities .box .main{
        font-size: 1.4rem;
        font-weight: bold;
        padding: 15px 0 ;

    }
    .what.activities .box .sub{
        font-size: 1.4rem;

    }
    .what.activities .box .date{
        font-size: 1.4rem;
        padding: 5px 0;
    }
    .what.activities .box .date span{
        /*border-bottom:1px dotted #FF2744;*/
         padding: 5px 20px 5px 0;
        color: #999;
    }
    .what.activities .box .more{
        position: absolute;
        bottom: 0;
        padding: 20px 0px;
        color: #FF2744;
    }
       .what.activities  h1{
       font-size: 3.4rem;
        text-align: center;
        padding: 2rem 0;
    }
       .what.activities  h1 span{
      opacity: 0.6;
    }
</style>

<!--<div class="what container intro">
    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12"> 
        <div class="box "> 
            <div class="main">Attività</div>    
                        <div class="sub">
                            Mi occupo solo di diritto del lavoro.<br>
                            Presto assistenza sia ai datori di lavoro, italiani e stranieri, sia alle persone fisiche (dirigenti, quadri, impiegati, operai e agenti di commercio), offrendo un supporto altamente specializzato in ogni esigenza di gestione delle Risorse Umane.<br>
                            L'assistenza si svolge nella consulenza day-by-day o nella gestione di singole azioni o problemi complessi: a monte dei problemi per evitare che essi sorgano (il miglior modo per evitare una lite è la stipulazione di un buon contratto; il miglior modo per non perderla è gestirla in modo professionale fin dall'inizio); nelle controversie insorte operando per la ricerca di una soluzione condivisa tra le parti, evitando fin dove è possibile i rischi e i costi di una lite giudiziaria; in giudizio per la tutela dei nostri clienti e dei loro interessi ogni qualvolta ciò sia necessario o opportuno.   
                        </div>  
        </div>
    </div>
</div>-->
<div class="what activities row">
    <h1>Ultimi <span>Aggiornamenti</span> </h1>
    <?php
    foreach ($latestNews as $news)
    {
//                print_r($news);
        ?>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3"> 
            <div class="box"> 
                <div class="date">
                    <i class="far fa-calendar" aria-hidden="true"></i>
                    <span><?php echo date('d F Y', strtotime($news->created)); ?></span>
                </div>    
                <div class="main">
                    <?php $diff = 250 - strlen($news->title); ?>
                    <?php echo $news->title; ?>
                </div>    
                <div class="sub">
                    <?php echo substr($news->introtext, 0, $diff) . '...'; ?>
                </div>    
                <div class="more">
                     {article <?php echo $news->id; ?>}[link]<?php echo JText::_("MOD_MASTER_READ_MORE"); ?>
                        <i class="fa fa-angle-double-right"></i>[/link]{/article}
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="clear"></div>
</div>


