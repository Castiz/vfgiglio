<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
<style>
    .what2{ padding: 4rem 0}
    .what2 .box {
        /*padding: 30px ;*/
        margin: 5px 0;
        color: #333;
        min-height: 250px;
    }
    .what2 .box .main{
        font-size: 3.2rem;
        /*border-bottom: 1px solid #B21B16;*/
        padding: 66px  0;
        text-align: center;
        color: #fff;
        background: #3D3C3F;
        position: relative;
    }
    .what2 .box .main .title{
        
    
    }
    .what2 .box .main img{
        position: absolute;
        top: 0;
        opacity: 0.1;
    }
    .what2 .box .sub{
        font-size: 1.6rem;
        padding: 10px 0;
        background: #fff;
        z-index: 101;
        position: relative;
    }
    .what2 .box .sub a{
        color: #B21B16;
    }
</style>

<!--<div class="what2  container">
    <div class="box"> 
        <div class="col-xs-12 col-sm-6 col-md-7 col-xl-7"> 

            <div class="sub"> {article 1}[introtext]{/article} </div>  
            <div class="more">
                {article 3}[link]<?php echo JText::_("MOD_MASTER_READ_MORE"); ?>
                <i class="fa fa-angle-double-right"></i>[/link]{/article} 
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5 col-xl-5"> 
            <div class="main"> {article 1}[title]{/article} </div>    
           <img class="img-responsive" src="/images/homepage/organizzazione.jpg" alt="curriculum"/>
        </div>
    </div>
    <div class="clear"></div>
    <div class="box"> 
        <div class="col-xs-12 col-sm-6 col-md-5 col-xl-5"> 
            <div class="main">{article 2}[title]{/article}</div>    

            <img class="img-responsive" src="/images/homepage/organizzazione.jpg" alt="area_professionale"/>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-7 col-xl-7"> 
            <div class="sub"> {article 2}[introtext]{/article} </div>
            <div class="more">
                {article 3}[link]<?php echo JText::_("MOD_MASTER_READ_MORE"); ?>
                <i class="fa fa-angle-double-right"></i>[/link]{/article} 
            </div>
        </div>
    </div>
    <div class="box"> 
        <div class="col-xs-12 col-sm-6 col-md-7 col-xl-8"> 
            <div class="sub"> {article 3}[introtext]{/article} </div>  
            <div class="more">
                {article 3}[link]<?php echo JText::_("MOD_MASTER_READ_MORE"); ?>
                <i class="fa fa-angle-double-right"></i>[/link]{/article} 
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5 col-xl-5"> 
            <div class="main">{article 3}[title]{/article}</div>    

            <img class="img-responsive" src="/images/homepage/organizzazione.jpg" alt="organizzazione"/>
        </div>
    </div>
</div>-->

<div class="what2  container">

    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-4"> 
        <div class="box"> 
            <div class="main"> 
                <div class="title">{article 1}[title]{/article} </div>
                <img class="img-responsive" src="/images/homepage/curriculum.jpg" alt="curriculum"/>
            </div>
            <div class="sub"> {article 1}[introtext]{/article} 
                <div class="more">
                    {article 1}[link]<?php echo JText::_("MOD_MASTER_READ_MORE"); ?>
                    <i class="fa fa-angle-double-right"></i>[/link]{/article} 
                </div>
            </div> 
        </div>
        <!--        <div class="col-xs-12 col-sm-6 col-md-5 col-xl-5"> 
                    <div class="main"> {article 1}[title]{/article} </div>    
                   <img class="img-responsive" src="/images/homepage/organizzazione.jpg" alt="curriculum"/>
                </div>-->
    </div>

    <!--        <div class="col-xs-12 col-sm-6 col-md-5 col-xl-5"> 
                <div class="main">{article 2}[title]{/article}</div>    
    
                <img class="img-responsive" src="/images/homepage/organizzazione.jpg" alt="area_professionale"/>
            </div>-->
    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-4"> 
        <div class="box"> 
            <div class="main"> 
                <div class="title">{article 2}[title]{/article} </div>
                <img class="img-responsive" src="/images/homepage/organizzazione.jpg" alt="organizzazione"/>
            </div>
            <div class="sub"> {article 2}[introtext]{/article} 
                <div class="more">
                    {article 2}[link]<?php echo JText::_("MOD_MASTER_READ_MORE"); ?>
                    <i class="fa fa-angle-double-right"></i>[/link]{/article} 
                </div>
            </div> 
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4 col-xl-4"> 
        <div class="box"> 
            <div class="main">
                <div class="title">{article 3}[title]{/article} </div>
                <img class="img-responsive" src="/images/homepage/area_professionale.jpg" alt="area_professionale"/>
            </div>    
            <div class="sub"> {article 3}[introtext]{/article}  
                <div class="more">
                    {article 3}[link]<?php echo JText::_("MOD_MASTER_READ_MORE"); ?>
                    <i class="fa fa-angle-double-right"></i>[/link]{/article} 
                </div>
            </div> 
        </div>
        <!--        <div class="col-xs-12 col-sm-6 col-md-5 col-xl-5"> 
                    <div class="main">{article 3}[title]{/article}</div>    
        
                    <img class="img-responsive" src="/images/homepage/organizzazione.jpg" alt="organizzazione"/>
                </div>-->
    </div>
    <div class="clear"></div>
</div>
