<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
<style>
    .legal{
        text-align: center;
        padding: 1rem;
        border-top: 2px solid #333;
    }
</style>

 
<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 legal "> 
    © 2019 - Avv. Vincenzo Fabrizio Giglio – Studio legale del Lavoro – P.IVA 02043780812<br>
    <a  href="<?php echo JRoute::_('index.php?option=com_content&view=article&id=5987&Itemid=106', true, -1); ?>">Note legali </a> | <a target="_blank" href="https://www.iubenda.com/privacy-policy/83384912">Privacy policy</a>
</div>
