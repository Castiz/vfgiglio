<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

$active = JFactory::getApplication()->getMenu()->getActive();
?>

<div class="header">

    <div class="header_title"> <span><?php echo $active->title; ?></span></div>
    <?php if ($active->id == 105): ?>
    <iframe class="embed-responsive-item" style="width:100%" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5596.619388838502!2d9.167467!3d45.463565!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4786c15967eb42d5%3A0x396ee3152eb0949c!2sVia+San+Vittore%2C+45%2C+20123+Milano+MI%2C+Italy!5e0!3m2!1sen!2sus!4v1556810940527!5m2!1sen!2sus" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    <?php else: ?>
        <img class="img-responsive" src="/images/headers/<?php echo $active->id; ?>.jpg" alt="<?php echo $active->title; ?>" />
    <?php endif; ?>
</div>